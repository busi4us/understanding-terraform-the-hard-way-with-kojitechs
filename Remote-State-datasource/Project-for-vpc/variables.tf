

variable "vpc_cidr" {
  type        = string
  description = "The value for the vpc cidr"
}

variable "component" {
  type        = string
  description = "Name of the project we are working on"
  default     = "project-for-ec2"
}

variable "public_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

# export TF_VAR_public_subnetcidr='["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]'

variable "private_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

variable "database_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

